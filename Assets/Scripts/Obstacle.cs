﻿using UnityEngine;

public abstract class Obstacle : MonoBehaviour
{
    public bool isDestroyable = true;

    void OnTriggerEnter(Collider other)
    {
        if (isDestroyable && other.name == "PlayerProjectile")
        {
            print("Destroying " + name);
            Destroy(gameObject);

            // TODO: Explode

            // Reduce speed of ball that hit us
            Rigidbody otherBody = other.gameObject.GetComponent<Rigidbody>();
            otherBody.AddForce(new Vector3(-5, 0, 0), ForceMode.Impulse);
        }
    }


}

﻿using UnityEngine;
using System.Collections;

public class Tank : Obstacle {

    public bool firesAtPlayer = true;

    private GameObject player;

    private int fireIndex = 0;
    private float lastFireTime = 0;

    public enum ProjectileForce
    {
        Low, High
    }

    public GameObject cannonballPrefab;

	// Use this for initialization
	void Start () {
        player = GameObject.Find("PlayerProjectile");
	}
	
	// Update is called once per frame
	void FixedUpdate () {
	    if (firesAtPlayer && player != null && transform.position.x - player.transform.position.x < 3)
        {
            if (fireIndex % 3 == 0)
            {
                if (Time.time - lastFireTime > 0.2f)
                {
                    FireCannon(ProjectileForce.Low);
                }
            }
            else if (Time.time - lastFireTime > 0.1f)
            {
                FireCannon(ProjectileForce.Low);
            }
        }
	}

    /**
     * Fires the cannon and returns the new GameObject that was added to the scene
     */
    public GameObject FireCannon(ProjectileForce force)
    {
        // Record this projectile being fired
        fireIndex++;
        lastFireTime = Time.time;

        // Create a new projectile at the muzzle of the cannon
        GameObject ball = (GameObject) GameObject.Instantiate(cannonballPrefab, transform.position, transform.rotation);
        ball.transform.Translate(new Vector3(0, 0.8f, 0.5f));

        // Give it an initial force based on user input
        Rigidbody rigidbody = ball.GetComponent<Rigidbody>();
        float initialForce;
        switch (force)
        {
            default:
            case ProjectileForce.Low:   initialForce = Random.Range(13.0f, 16.0f);   break;
            case ProjectileForce.High:  initialForce = Random.Range(22.0f, 28.0f);  break;
        }

        initialForce *= rigidbody.mass;
        float angle = 15f * Mathf.Deg2Rad;
        rigidbody.AddForceAtPosition(new Vector3(initialForce * Mathf.Cos(angle), initialForce * Mathf.Sin(angle), 0),
                                     new Vector3(1, 0),
                                     ForceMode.Impulse);
        
        // TODO: Smoke, muzzle flare, etc

        return ball;
    }
}

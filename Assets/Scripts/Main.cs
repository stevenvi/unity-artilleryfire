﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class Main : MonoBehaviour {

    // Constants
    private const int GROUND_WIDTH = 6;
    private const int MIN_OBSTACLE_SEPARATION = 20;
    private const int MAX_OBSTACLE_SEPARATION = 30;
    private const int GAME_PLANE_Z = -5;

    // Prefabs
    public GameObject groundPrefab;
    public GameObject tankPrefab;
    public GameObject barracksPrefab;

    // UI objects
    private Text playerMessage;
    private Text distanceText;

    // Gameplay variables
    private GameState gameState = GameState.GameOver;
    private GameObject[] ground;
    private GameObject playerTank;
    private GameObject playerProjectile;
    private Rigidbody playerProjectileRB;
    private List<GameObject> obstacles;
    private float gameOverStartTime = 0;

	// Use this for initialization
	void Start () {
        print("Game Startup");

        // Fetch UI objects
        playerMessage = GameObject.Find("PlayerMessage").GetComponent<Text>();
        distanceText = GameObject.Find("DistanceText").GetComponent<Text>();

        Restart();
	}
    private float x = 0;
	
	// Update is called once per frame
    void Update()
    {
        // Snap camera to the player's cannonball
        // TODO: Tween this rather than having it snapped so tightly
        if (playerProjectile != null)
        {
            Camera camera = GetComponent<Camera>();
            float dx = playerProjectile.transform.position.x - camera.transform.position.x;
            camera.transform.Translate(new Vector3(dx + 2, 0, 0));
        }
    }

    void FixedUpdate()
    {
        switch (gameState)
        {
            case GameState.WaitingToFire:
                if (Input.anyKeyDown)
                {
                    playerProjectile = playerTank.GetComponent<Tank>().FireCannon(Tank.ProjectileForce.High);
                    playerProjectile.name = "PlayerProjectile";
                    playerProjectile.GetComponent<Renderer>().material.color = Color.black;
                    playerProjectileRB = playerProjectile.GetComponent<Rigidbody>();
                    ChangeGameState(GameState.Gameplay);
                }
                break;

            case GameState.Gameplay:

                // Update the distance text
                distanceText.text = string.Format("Distance: {0:0.0} m", playerProjectile.transform.position.x + playerTank.transform.position.x);

                // Move the ground[0] forward if the player is above the ground[1] object
                if (playerProjectile.transform.position.x > ground[1].transform.position.x - GROUND_WIDTH / 2)
                {
                    ground[0].transform.Translate(new Vector3(GROUND_WIDTH * 2, 0, 0));
                    GameObject temp = ground[0];
                    ground[0] = ground[1];
                    ground[1] = temp;
                }

                if (Input.anyKeyDown)
                {
                    if (Mathf.Abs(playerProjectileRB.velocity.y) < 1)
                    {
                        // This will allow the player to jump at the apex of their parabola as well
                        // Sure, that might be fun, why not, right?
                        Vector3 v = playerProjectileRB.velocity;
                        v.y = 3.5f + v.x * 0.25f;
                        playerProjectileRB.velocity = v;
                        //playerProjectileRB.AddForce(new Vector3(0, 100f + playerProjectileRB.velocity.x, 0), ForceMode.Acceleration);
                    }
                }

                // If velocity is about zero, set it to zero and move to GameOver state
                if (playerProjectileRB.velocity.magnitude < 0.1f || playerProjectileRB.velocity.x < 0.0f)
                {
                    playerProjectileRB.velocity = new Vector3();
                    ChangeGameState(GameState.GameOver);
                }

                ManageObstacles();
                break;

            case GameState.GameOver:
                if (Time.time - gameOverStartTime > 0.5f && Input.anyKey)
                {
                    // TODO: Add a cool tween here to let the player review their destruction?
                    Restart();
                }
                break;
        }
    }

    private void Restart()
    {
        // Remove any on-screen objects
        if (playerProjectile != null)
        {
            Destroy(playerProjectile);
        }

        if (obstacles != null)
        {
            foreach (GameObject go in obstacles)
            {
                Destroy(go);
            }
        }
        obstacles = new List<GameObject>();

        // Create the ground
        if (ground == null)
        {
            ground = new GameObject[] { GameObject.Instantiate(groundPrefab), GameObject.Instantiate(groundPrefab) };
        }
        ground[0].transform.position = new Vector3(0, 0, 0);
        ground[1].transform.position = new Vector3(GROUND_WIDTH, 0, 0);

        // Create the player
        if (playerTank == null)
        {
            playerTank = GameObject.Instantiate(tankPrefab);
            playerTank.transform.position = new Vector3(-1.5f, 0, GAME_PLANE_Z);
            Tank tank = playerTank.GetComponent<Tank>();
            tank.isDestroyable = false;
            tank.firesAtPlayer = false;
        }

        // Reset the camera
        Camera camera = GetComponent<Camera>();
        camera.transform.position = new Vector3(0, 1, -10);

        ChangeGameState(GameState.WaitingToFire);
    }

    private void ChangeGameState(GameState newGameState)
    {
        if (newGameState != gameState)
        {
            // Change the state
            print("New game state: " + newGameState);
            gameState = newGameState;

            // Perform any state-specific change operations here
            switch (gameState)
            {
                case GameState.WaitingToFire:
                    playerMessage.text = "Tap to play!";
                    distanceText.text = "Distance: 0.0 m";
                    break;

                case GameState.Gameplay:
                    playerMessage.text = "";
                    break;

                case GameState.GameOver:
                    playerMessage.text = "Game Over";
                    gameOverStartTime = Time.time;
                    break;
            }
        }
    }

    // Deletes old obstacles
    private void ManageObstacles()
    {
        // Trim off any obstacles that have been destroyed by the player
        for (int i = obstacles.Count - 1; i >= 0; --i)
        {
            if (obstacles[i] == null)
            {
                obstacles.RemoveAt(i);
            }
        }

        // Was the last obstacle offscreen?
        bool removedObstacle = false;
        
        if (obstacles.Count > 0 && obstacles[0].transform.position.x + 5 < playerProjectile.transform.position.x)
        {
            // Remove the old one
            Destroy(obstacles[0]);
            obstacles.RemoveAt(0);
            removedObstacle = true;
        }

        if (removedObstacle || obstacles.Count == 0) {
            // Add a new one
            float lastObstacleX = obstacles.Count > 0 ? obstacles[obstacles.Count - 1].transform.position.x : playerProjectile.transform.position.x;
            float nextObstacleX = lastObstacleX + Random.Range(MIN_OBSTACLE_SEPARATION, MAX_OBSTACLE_SEPARATION);
            print("Adding obstacle at " + nextObstacleX);

            // Select randomly a tank or a barracks
            GameObject newTank = GameObject.Instantiate(Random.value > 0.5f ? tankPrefab : barracksPrefab);
            newTank.transform.position = new Vector3(nextObstacleX, 0, GAME_PLANE_Z);
            obstacles.Add(newTank);
        }

        // 
    }

}

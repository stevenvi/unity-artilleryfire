﻿public enum GameState {
    WaitingToFire,
    Gameplay,
    GameOver
}

﻿using UnityEngine;
using System.Collections;

public class Cannonball : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
	    if (transform.position.y < 0)
        {
            // Fell off the edge of the world, kill it
            print("Killing ball");
            Destroy(gameObject);
        }
	}

    void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.gameObject.GetComponent<Cannonball>() != null)
        {
            // We hit another cannonball!
            // Only the player should apply the speed boost
            if (collision.collider.gameObject.transform.position.x < transform.position.x)
            {
                if (name == "PlayerProjectile")
                {
                    print("Speed boost!");
                    GetComponent<Rigidbody>().AddForce(new Vector3(25, 0, 0), ForceMode.Impulse);
                }
                else
                {
                    Rigidbody rb = GetComponent<Rigidbody>();
                    Vector3 v = rb.velocity;
                    rb.velocity = new Vector3(v.x * 0.8f, v.y * 0.8f, v.z * 0.8f);
                }
            }
        }
    }

}
